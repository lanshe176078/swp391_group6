Create Database SWP391_Group6
-- Bảng Categories
CREATE TABLE Categories (
   cid INT PRIMARY KEY IDENTITY(1,1),
    cname NVARCHAR(150),
);

-- Bảng Products
CREATE TABLE Products (
  pid INT PRIMARY KEY IDENTITY(1,1),
	cid int,
    pname NVARCHAR(50),
    rate FLOAT,
    img NVARCHAR(MAX),
	img2 NVARCHAR(MAX),
    price INTEGER,
	brandid INT,
    priceSale INTEGER,
    quantity INTEGER,
    isDiscount BIT,
    isSoldout BIT,
    created_at NCHAR(10),
    status INT,
	FOREIGN KEY (cid) REFERENCES Categories (cid),
	FOREIGN KEY (brandid) REFERENCES Brand (brandid)
);
-- Bảng Brand
CREATE TABLE Brand (
    brandid INT PRIMARY KEY IDENTITY(1,1),
    brandname NVARCHAR(50) NOT NULL
    -- Các cột khác nếu cần
);

-- Bảng AccountStatuses
CREATE TABLE AccountStatuses (
     status_id INT PRIMARY KEY IDENTITY(1,1),
    status_name NVARCHAR(50) NOT NULL
    -- Các cột khác nếu cần
);

-- Bảng Accounts
CREATE TABLE Accounts (
     acid INT PRIMARY KEY IDENTITY(1,1),
    email NVARCHAR(50),
    emailConfirm BIT,
    password NVARCHAR(50),
    username NVARCHAR(50),
    role_id INT,
    created_at TIMESTAMP,
    status_id INT,
    FOREIGN KEY (role_id) REFERENCES Roles(role_id),
    FOREIGN KEY (status_id) REFERENCES AccountStatuses(status_id)
);

-- Bảng Roles
CREATE TABLE Roles (
    role_id INT PRIMARY KEY IDENTITY(1,1),
    role_name NVARCHAR(50) NOT NULL
    -- Các cột khác nếu cần
);

-- Bảng orderDetails
CREATE TABLE orderDetails (
     odid INT PRIMARY KEY IDENTITY(1,1),
    pid INT,
    oid INT,
    price FLOAT,
    quantity INT,
    FOREIGN KEY (pid) REFERENCES Products(pid),
    FOREIGN KEY (oid) REFERENCES orders(oid)
);

-- Bảng orders
CREATE TABLE orders (
	oid INT IDENTITY(1,1),
    acid INT,
    ordered_at FLOAT,
    TotalAmount INT,
    created_by NCHAR(10),
    address NVARCHAR(50),
    phone_number NCHAR(10),
    status INT,
    note NVARCHAR(250),
    receiver NVARCHAR(50),
    discount INT,
    PRIMARY KEY (oid),
    FOREIGN KEY (acid) REFERENCES Accounts(acid)
);

CREATE TABLE Cart (
     cart_id INT PRIMARY KEY IDENTITY(1,1),
    acid INT,
    total_order_price DECIMAL(10, 2) DEFAULT 0.0,
    FOREIGN KEY (acid) REFERENCES Accounts(acid)

);
CREATE TABLE CartDetails (
     cart_details_id INT PRIMARY KEY IDENTITY(1,1),
    cart_id INT,
    pid INT,
    quantity INT,
    total_cost DECIMAL(10, 2),
    FOREIGN KEY (cart_id) REFERENCES Cart(cart_id),
    FOREIGN KEY (pid) REFERENCES Products(pid)
);
--Bảng banner
CREATE TABLE Banner (
     banid INT PRIMARY KEY IDENTITY(1,1),
    img NVARCHAR(150),
    status INTEGER,
    created_at TIMESTAMP,
    role_id INT, -- Thêm cột role_id
    FOREIGN KEY (role_id) REFERENCES Roles(role_id)
);
-- Bảng CodeSale
CREATE TABLE CodeSale (
     csid INT PRIMARY KEY IDENTITY(1,1),
    codeSale NVARCHAR(50),
    discount INTEGER,
    csStatus INTEGER,
    dateStart NCHAR(10),
    dateEnd NCHAR(10),
    limitedQuantity INT,
    discountConditions NVARCHAR(MAX),
    title NCHAR(100),
    role_id INT,
    FOREIGN KEY (role_id) REFERENCES Roles(role_id)
);

-- Bảng ProductDetail
CREATE TABLE ProductDetail (
     pdid INT PRIMARY KEY IDENTITY(1,1),
    size NVARCHAR(MAX),
    color NVARCHAR(MAX),
    gender BIT,
    description NVARCHAR(MAX),
    pid INTEGER,
    FOREIGN KEY (pid) REFERENCES Products(pid)
);

-- Bảng Product_CS (Bảng trung gian)
CREATE TABLE Product_CS (
   pcs_id INT PRIMARY KEY IDENTITY(1,1),
    pid INTEGER,
    size NVARCHAR(MAX),
    color NVARCHAR(MAX),
    FOREIGN KEY (pid) REFERENCES Products(pid)
);

-- Bảng Feedback
CREATE TABLE Feedback (
    feedback_id INT PRIMARY KEY IDENTITY(1,1),
    acid INT,
    pid INT,
    rating INTEGER,
    comment NVARCHAR(MAX),
    created_at TIMESTAMP,
    FOREIGN KEY (acid) REFERENCES Accounts(acid),
    FOREIGN KEY (pid) REFERENCES Products(pid)
);
